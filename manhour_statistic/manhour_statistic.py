import logging
import numpy as np
import pandas as pd
from pandas import Timedelta
from optparse import OptionParser

logging.basicConfig(format='%(asctime)s %(name)-5s %(levelname)-5s %(message)s')
logger = logging.getLogger("工时统计")
logger.setLevel(logging.INFO)


class TimeData:
    def __init__(self, origin_excel, target_in_building_excel, target_in_room_excel, out_time_excel):
        self.card_machine_in_building_list = ["AC-6_D-4F-01进_RD-4F-01进", "AC-6_D-4F-02进_RD-4F-02进",
                                              "AC-12_D-4F-16进_RD-4F-16进", "AC-12_D-4F-15进_RD-4F-15进",
                                              "AC-8_D-4F-17_RD-4F-17进", "RD-4F-02进", "RD-4F-01进",
                                              "RD-4F-15进", "RD-4F-17进"]
        self.card_machine_out_building_list = ["AC-6_D-4F-01出_RD-4F-01出", "AC-6_D-4F-02出_RD-4F-02出",
                                               "AC-12_D-4F-16出_RD-4F-16出", "AC-12_D-4F-15出_RD-4F-15出",
                                               "AC-8_D-4F-17_RD-4F-17出", "RD-4F-02出", "RD-4F-01出",
                                               "RD-4F-15出", "RD-4F-17出"]
        self.card_machine_in_room_list = ["AC-7_D-4F-07_RD-4F-07进", "AC-8_D-4F-17_RD-4F-17进",
                                          "AC-1_D-3F-01_RD-3F-01进"]
        self.card_machine_out_room_list = ["AC-7_D-4F-07_RD-4F-07出", "AC-8_D-4F-17_RD-4F-17出",
                                           "AC-1_D-3F-01_RD-3F-01出"]
        self.card_machine_other_floor = ["AC-1_D-3F-01_RD-3F-01进", "AC-1_D-3F-01_RD-3F-01出"]
        self.__origin_excel = origin_excel
        self.__target_in_building = target_in_building_excel
        self.__target_in_room = target_in_room_excel
        self.__out_building_time_statistic = out_time_excel
        self.__df = self.read_excel()
        self.__df = self.format_table()
        self.__data_length = self.__df.shape[0]
        self.__df['time'] = pd.to_datetime(self.df['事件时间'])
        self.__df['day'] = self.df['time'].dt.strftime('%D')
        self.__duration_in_building = {}
        self.__duration_in_room = {}
        self.__data_in_building_list = []
        self.__data_in_room_list = []
        self.__out_building_time_list = []
        self.__data_out_building_time_list = []

    @property
    def origin_excel(self):
        return self.__origin_excel

    @property
    def target_in_building(self):
        return self.__target_in_building

    @property
    def target_in_room(self):
        return self.__target_in_room

    @property
    def df(self):
        return self.__df

    @property
    def data_length(self):
        return self.__data_length

    @property
    def duration_in_building(self):
        return self.__duration_in_building

    @property
    def duration_in_room(self):
        return self.__duration_in_room

    @property
    def data_in_building_list(self):
        return self.__data_in_building_list

    @property
    def data_in_room_list(self):
        return self.__data_in_room_list

    @property
    def out_building_time_list(self):
        return self.__out_building_time_list

    @staticmethod
    def highlight_start_time(value):
        """
        :param value: 表格里面的元素, 如果大于中午12点, 就高亮显示
        :return:
        """
        if isinstance(value, str):
            return 'background-color: red'
        elif value > pd.to_datetime("12:00:00 pm").timetz():
            return 'background-color: yellow'
        else:
            return 'background-color: white'

    @staticmethod
    def highlight_stop_time(value):
        """
        :param value: 表格里面的元素, 如果下午下班时间超过7点半, 就高亮显示
        :return:
        """
        if isinstance(value, str):
            return 'background-color: red'
        elif value > pd.to_datetime("19:30:00").timetz():
            return 'background-color: yellow'
        else:
            return 'background-color: white'

    @staticmethod
    def highlight_worktime(value):
        """
        :param value: 表格里面的元素,工作时长, 如果小于8小时, 就高亮显示
        :return:
        """
        if not isinstance(value, float):
            return 'background-color: red'
        elif value < 8.0:
            return 'background-color: yellow'
        else:
            return 'background-color: white'

    @staticmethod
    def add_log(func):
        def wrapper(*args, **kw):
            logger.info(f"开始执行 {func.__name__}")
            return func(*args, **kw)

        return wrapper

    @add_log
    def read_excel(self):
        """
        从原始数据excel中读取
        :return: 原始数据
        """
        df = pd.read_excel(self.origin_excel, sheet_name=0)
        return df

    @add_log
    def write_sheet(self, df, target_excel_file):
        """
        :param df: 要写入的数据表格
        :param target_excel_file: 目标excel 文件
        :return: na
        """
        # 给符合条件的高亮显示
        df.style.applymap(self.highlight_worktime, subset=pd.IndexSlice[['工作时长']]). \
            applymap(self.highlight_start_time, subset=pd.IndexSlice[['上班时间']]). \
            applymap(self.highlight_stop_time, subset=pd.IndexSlice[['下班时间']]). \
            to_excel(target_excel_file, engine='openpyxl')
        print(df)

    def sort_date(self, df):
        """
        等待补充, 如果数据很乱的话
        :param df:
        :return:
        """
        pass

    def format_table(self):
        """
        删除不必要的列, 只保留有用的数据, 这样避免不同的数据列导致的影响
        删除其他楼层的刷卡信息, 导致的整体错误
        :return:
        """
        # 删除不必要的列
        for x in self.__df:
            if x not in ["卡号", "持卡人员", "事件时间", "事件源"]:
                self.__df = self.__df.drop(labels=x, axis=1)
        # 删除其他楼层的刷卡信息
        other_floor_rows = []
        for row in range(self.__df.shape[0] - 2):
            if self.__df.iat[row, 3] in self.card_machine_other_floor:
                other_floor_rows.append(row)
        self.__df = self.__df.drop(labels=other_floor_rows, axis=0)
        return self.__df.sort_values(['持卡人员', '事件时间'])

    @add_log
    def data_handling_in_room(self):
        """
        从原始数据中遍历, 记录进入房间和出房间的时间, 并计算, 重新写到新的表格中
        :return: none
        """
        for row in range(self.data_length):
            self.duration_in_room[self.df.iat[row, 0]] = {}

        swipe_cards_time = []
        try:
            for row in range(self.data_length):
                # 计算在工作房间里面的时间
                if self.df.iat[row, 3] in self.card_machine_in_room_list or \
                        self.df.iat[row, 3] in self.card_machine_out_room_list:
                    swipe_cards_time.append((self.df.iat[row, 2], self.df.iat[row, 3]))
                    self.duration_in_room[self.df.iat[row, 0]].update({self.df.iat[row, 5]: swipe_cards_time})

                if row < self.data_length - 1 and (
                        self.df.iat[row, 5] != self.df.iat[row + 1, 5] or
                        self.df.iat[row, 0] != self.df.iat[row + 1, 0]) or \
                        row == self.data_length - 1:
                    swipe_cards_time = []
                    if self.duration_in_room[self.df.iat[row, 0]] == {}:
                        continue
                    if self.duration_in_room[self.df.iat[row, 0]][self.df.iat[row, 5]][0][1] \
                            in self.card_machine_in_room_list:
                        time_in_room = self.duration_in_room[self.df.iat[row, 0]][self.df.iat[row, 5]][0][0]
                    else:
                        time_in_room = "no record"
                    if self.duration_in_room[self.df.iat[row, 0]][self.df.iat[row, 5]][-1][1] \
                            in self.card_machine_out_room_list:
                        time_out_room = self.duration_in_room[self.df.iat[row, 0]][self.df.iat[row, 5]][-1][0]
                    else:
                        time_out_room = "no record"
                    if time_in_room == "no record" or time_out_room == "no record":
                        during_room = "no record"
                        logger.error(f"{self.df.iat[row, 1]} forgot to swipe ID card on {self.df.iat[row, 5]} when"
                                     f"enter into the room")
                    else:
                        # 如果中午12点之后来上班就不用减去午饭1小时时间
                        if time_in_room < pd.to_datetime(f"{self.df.iat[row, 5]} 12:00:00 pm"):
                            during_room = float("%.2f" % ((time_out_room - time_in_room - Timedelta("01:00:00")) /
                                                          np.timedelta64(1, 'h')))
                        else:
                            during_room = float("%.2f" % ((time_out_room - time_in_room) / np.timedelta64(1, 'h')))
                    if not isinstance(time_in_room, str):
                        time_in_room = time_in_room.timetz()
                    if not isinstance(time_out_room, str):
                        time_out_room = time_out_room.timetz()
                    # 生成新的表格, 准备写入excel
                    self.data_in_room_list.append({"姓名": self.df.iat[row, 1],
                                                   "日期": self.df.iat[row, 5],
                                                   "上班时间": time_in_room,
                                                   "下班时间": time_out_room,
                                                   "工作时长": during_room})
        except Exception as e:
            print(e)
        # logger.info(self.data_in_room_list)
        # 集合所有数据, 目前只有一个表
        total_data = pd.concat([pd.DataFrame(self.data_in_room_list)], axis=0)
        # 写入表格
        self.write_sheet(total_data, self.target_in_room)

    @add_log
    def data_handling_in_building(self):
        """
        从原始数据中遍历每一条记录, 并记录进入公司和出公司的时间,并计算,重新写到新的表格中
        :return: none
        """
        for row in range(self.data_length):
            self.duration_in_building[self.df.iat[row, 0]] = {}

        try:
            for row in range(self.data_length):
                # 计算在大楼里面的时间
                # 如果时间和上一行不一样表示是新的一天的开始
                if row == 0 or \
                        self.df.iat[row, 5] != self.df.iat[row - 1, 5] or \
                        self.df.iat[row, 0] != self.df.iat[row - 1, 0]:
                    time_in_building = pd.to_datetime(self.df.iat[row, 2]) \
                        if self.df.iat[row, 3] in self.card_machine_in_building_list else "no record"
                # 如果时间和下一行不一样表示是一天的结束
                if row < self.data_length - 1 and (
                        self.df.iat[row, 5] != self.df.iat[row + 1, 5] or
                        self.df.iat[row, 0] != self.df.iat[row + 1, 0]) or \
                        row == self.data_length - 1:
                    time_out_building = pd.to_datetime(self.df.iat[row, 2]) \
                        if self.df.iat[row, 3] in self.card_machine_out_building_list else "no record"
                    if time_in_building == "no record" or time_out_building == "no record":
                        during_building = "no record"
                        logger.error(f"{self.df.iat[row, 0]} forgot to swipe ID card on {self.df.iat[row, 5]} when "
                                     f"enter into the building")
                    else:
                        # 如果中午12点之后来上班就不用减去午饭1小时时间
                        if time_in_building < pd.to_datetime(f"{self.df.iat[row, 5]} 12:00:00 pm"):
                            self.duration_in_building[self.df.iat[row, 0]][self.df.iat[row, 5]] = \
                                "%.2f" % ((time_out_building - time_in_building - Timedelta("01:00:00")) /
                                          np.timedelta64(1, 'h'))
                        else:
                            self.duration_in_building[self.df.iat[row, 0]][self.df.iat[row, 5]] = \
                                "%.2f" % ((time_out_building - time_in_building) / np.timedelta64(1, 'h'))
                        during_building = float(self.duration_in_building[self.df.iat[row, 0]][self.df.iat[row, 5]])
                    if not isinstance(time_in_building, str):
                        time_in_building = time_in_building.timetz()
                    if not isinstance(time_out_building, str):
                        time_out_building = time_out_building.timetz()
                    # 生成新的表格, 准备写入excel
                    self.data_in_building_list.append({"姓名": self.df.iat[row, 0],
                                                       "日期": self.df.iat[row, 5],
                                                       "上班时间": time_in_building,
                                                       "下班时间": time_out_building,
                                                       "工作时长": during_building})
        except Exception as e:
            print(e)

        # logger.info(data_in_building_list)
        # 集合所有数据, 目前只有一个表
        total_data = pd.concat([pd.DataFrame(self.data_in_building_list)], axis=0)
        # 写入表格
        self.write_sheet(total_data, self.target_in_building)

    @add_log
    def valid_data_handling_in_building(self):
        """
        计算方法：下班时间 减去 上班时间 再减去（午饭不足1小时，按照1小时，午饭超过1个小时，按照实际的时间计算）再减去 中间出4楼的总时间（多次出入的累计时间）
        :return:
        """
        all_break_time = list()
        break_time_greater_one_hour = ""
        time_in_building = "no record"
        all_break_time_fix = list()
        max_break_time = Timedelta("00:00:00")
        for row in range(self.data_length):
            self.duration_in_building[self.df.iat[row, 0]] = {}

        try:
            for row in range(self.data_length):
                # 计算在大楼里面的时间
                # 如果时间和上一行不一样表示是新的一天的开始
                if row == 0 or \
                        self.df.iat[row, 5] != self.df.iat[row - 1, 5] or \
                        self.df.iat[row, 0] != self.df.iat[row - 1, 0]:
                    # 第一条刷卡记录被视为进入公司时间，无论是出去的还是进来的
                    time_in_building = pd.to_datetime(self.df.iat[row, 2])
                    # if self.df.iat[row, 3] in self.card_machine_in_building_list else "no record"

                if self.df.iat[row, 3] in self.card_machine_in_building_list and \
                        self.df.iat[row, 5] == self.df.iat[row - 1, 5] and \
                        self.df.iat[row, 0] == self.df.iat[row - 1, 0]:
                    if self.df.iat[row-1, 3] in self.card_machine_out_building_list:
                        break_time = (pd.to_datetime(self.df.iat[row, 2]) - pd.to_datetime(self.df.iat[row - 1, 2]))
                        if pd.to_datetime(f"{self.df.iat[row, 5]} 01:10:00 pm") >= pd.to_datetime(self.df.iat[row, 2]) >= \
                                pd.to_datetime(f"{self.df.iat[row, 5]} 11:00:00 am") and \
                                pd.to_datetime(self.df.iat[row-1, 2]) > pd.to_datetime(f"{self.df.iat[row, 5]} 11:00:00 am"):
                            if max_break_time < break_time:
                                max_break_time = break_time
                        all_break_time.append(break_time)
                        # 如果某一段休息时间超过1小时， 高亮显示这一天， 并记录，以供查看
                        break_time_greater_one_hour = "True" if break_time / np.timedelta64(1, 'h') > 1.0 else ""
                    else:
                        logger.warning(f"{self.df.iat[row, 0]}在{self.df.iat[row, 5]}有连续进入大楼刷卡记录， 请复查")
                # print(all_break_time)
                # 如果时间和下一行不一样表示是一天的结束
                if row < self.data_length - 1 and (
                        self.df.iat[row, 5] != self.df.iat[row + 1, 5] or
                        self.df.iat[row, 0] != self.df.iat[row + 1, 0]) or \
                        row == self.data_length - 1:
                    # 最后一条记录被视为下班时间， 不管是进的还是出的
                    time_out_building = pd.to_datetime(self.df.iat[row, 2])
                    #     if self.df.iat[row, 3] in self.card_machine_out_building_list else "no record"
                    time_out_building_final = time_out_building
                    if time_out_building > pd.to_datetime(f"{self.df.iat[row, 5]} 7:30:00 pm"):
                        logger.warning(f"{self.df.iat[row, 0]} 在{self.df.iat[row, 5]} 超过19：30 下班， 请知悉")

                    # 把中途出去的时间全部减去， 不包括中途的一小时， 假设最长的时间是吃饭时间，算1小时
                    if all_break_time:
                        for b_time in all_break_time:
                            time_out_building = time_out_building - b_time
                        if Timedelta("01:00:00") > max_break_time > Timedelta("00:00:01") and \
                                time_in_building < pd.to_datetime(f"{self.df.iat[row, 5]} 12:00:00 pm") and \
                                time_out_building_final > pd.to_datetime(f"{self.df.iat[row, 5]} 02:00:00 pm"):
                            time_out_building = time_out_building - Timedelta("01:00:00") + max_break_time
                    else:
                        logger.warning(f"{self.df.iat[row, 0]}在{self.df.iat[row, 5]}中途没有出大楼记录，请复查")
                    all_break_time_fix = ["%.2f" % (x / np.timedelta64(1, 'h')) for x in all_break_time]
                    # if 438 < row < 457:
                    #     print(all_break_time_fix)
                    #     print(max_break_time)
                    #     # print()
                    # 如果中午12点之后来上班就不用减去午饭1小时时间
                    if time_in_building < pd.to_datetime(f"{self.df.iat[row, 5]} 12:00:00 pm") and \
                            time_out_building_final > pd.to_datetime(f"{self.df.iat[row, 5]} 02:00:00 pm"):
                        if max_break_time == Timedelta("00:00:00"):
                            lunch_time = Timedelta("01:00:00")
                        else:
                            lunch_time = Timedelta("00:00:00")
                        self.duration_in_building[self.df.iat[row, 0]][self.df.iat[row, 5]] = \
                            "%.2f" % ((time_out_building - time_in_building - lunch_time) /
                                      np.timedelta64(1, 'h'))
                        # if 438 < row < 457:
                        #     print(self.duration_in_building[self.df.iat[row, 0]][self.df.iat[row, 5]], lunch_time)
                    else:
                        self.duration_in_building[self.df.iat[row, 0]][self.df.iat[row, 5]] = \
                            "%.2f" % ((time_out_building - time_in_building) / np.timedelta64(1, 'h'))
                    during_building = float(self.duration_in_building[self.df.iat[row, 0]][self.df.iat[row, 5]])
                    if not isinstance(time_in_building, str):
                        time_in_building = time_in_building.timetz()
                    if not isinstance(time_out_building_final, str):
                        time_out_building_final = time_out_building_final.timetz()
                    # 生成新的表格, 准备写入excel
                    self.data_in_building_list.append({"姓名": self.df.iat[row, 0],
                                                       "日期": self.df.iat[row, 5],
                                                       "上班时间": time_in_building,
                                                       "下班时间": time_out_building_final,
                                                       "工作时长": during_building,
                                                       "外出时间": all_break_time_fix,
                                                       "最长外出时间>1h": break_time_greater_one_hour})
                    break_time_greater_one_hour = ""
                    all_break_time = []
                    max_break_time = Timedelta("00:00:00")
        except Exception as e:
            print(e)
        # logger.info(self.data_in_building_list)

        # 集合所有数据, 目前只有一个表
        # print(self.data_in_building_list)
        total_data = pd.concat([pd.DataFrame(self.data_in_building_list)], axis=0)
        # 写入表格
        self.write_sheet(total_data, self.target_in_building)

    @add_log
    def out_time(self, start_time, stop_time, time=1.0):
        """
        统计某一阶段出去的时间
        :return:
        """
        # for row in range(self.data_length):
        #     self.duration_in_building[self.df.iat[row, 0]] = {}

        try:
            out_building_time_list = list()
            for row in range(self.data_length):
                if start_time > self.df.iat[row, 2] > stop_time:
                    continue
                if self.df.iat[row, 3] in self.card_machine_in_building_list:
                    if self.df.iat[row - 1, 2] in self.card_machine_out_building_list:
                        break_time = (self.df.iat[row, 2] - self.df.iat[row - 1, 2]) / np.timedelta64(1, 'h')
                        print(break_time)
                    else:
                        logger.warning(f"{self.df.iat[row, 1]} lost the out record on {self.df.iat[row, 5]}")
                        continue
                    if break_time > time:
                        out_building_time_list.append(break_time)
                        print(out_building_time_list)

                # 如果时间和下一行不一样表示是一天的结束
                if row < self.data_length - 1 and (
                        self.df.iat[row, 5] != self.df.iat[row + 1, 5] or
                        self.df.iat[row, 0] != self.df.iat[row + 1, 0]) or \
                        row == self.data_length - 1:
                    self.out_building_time_list[self.df.iat[row, 0]][self.df.iat[row, 5]] = out_building_time_list

                    # 生成新的表格, 准备写入excel
                    self.__data_out_building_time_list.append({"姓名": self.df.iat[row, 1],
                                                               "日期": self.df.iat[row, 5],
                                                               "外出时间统计": self.out_building_time_list})
        except Exception as e:
            print(e)

        # logger.info(data_in_building_list)
        # 集合所有数据, 目前只有一个表
        total_data = pd.concat([pd.DataFrame(self.data_in_building_list)], axis=0)
        # 写入表格
        print(total_data)
        # total_data.to_excel(self.__data_out_building_time_list, engine='openpyxl')
        # self.write_sheet(total_data, self.__out_building_time_statistic)


def main():
    """
    main function
    :return:
    """
    # 传入两个参数, 原始数据, 和输出excel 统计表格
    logger.info("--开始统计--")
    parser = OptionParser()
    parser.add_option("-o", "--origin", dest="origin", help="original excel file of the man hour, you can specify"
                                                            " one path, also if you put it in the same path as python"
                                                            "file named 原始数据.xlsx, then you don't need this args")
    parser.add_option("-b", "--target_in_building",
                      dest="target_in_building", help="target excel file of man hour statistic, you can specify it, "
                                                      "and if you don't, it will save it in the same path as python "
                                                      "file named 工时统计.xlsx, it's for time in building")
    parser.add_option("-r", "--target_in_room",
                      dest="target_in_room", help="target excel file of man hour statistic, you can specify it, and if "
                                                  "you don't, it will save it in the same path as python file named "
                                                  "工时统计.xlsx it's for time in room")
    options, args = parser.parse_args()
    origin_excel = options.origin if options.origin else "原始数据.xlsx"
    target_in_building_excel = options.target_in_building if options.target_in_building else "大楼工时统计.xlsx"
    target_in_room_excel = options.target_in_room if options.target_in_room else "房间工时统计.xlsx"
    out_time_excel = "外出时间统计.xlsx"
    man_hour = TimeData(origin_excel, target_in_building_excel, target_in_room_excel, out_time_excel)
    # man_hour.data_handling_in_building()
    # man_hour.data_handling_in_room()
    man_hour.valid_data_handling_in_building()
    # man_hour.out_time(pd.to_datetime("7/27/2022  12:05:12 AM"), pd.to_datetime("7/27/2022  4:10:05 PM"))
    logger.info("--统计结束--")


if __name__ == '__main__':
    main()
